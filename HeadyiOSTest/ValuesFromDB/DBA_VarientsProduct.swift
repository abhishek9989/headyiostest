//
//  DBA_VarientsProduct.swift
//  HeadyiOSTest
//
//  Created by Angel Broking 4 on 24/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import UIKit


@objcMembers
class DBA_VarientsProduct: NSObject {

    var Id:Int
    var color:String
    var size :Int
    var price :Int
    var a_Product_id : Int
    
    init(Id:Int,color:String,size:Int,price:Int ,a_Product_id:Int )
    {
        self.Id = Id
        self.color = color
        self.size = size
        self.price = price
        self.a_Product_id = a_Product_id
    }
    
    
}
