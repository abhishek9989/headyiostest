//
//  DBA_Ranking.swift
//  HeadyiOSTest
//
//  Created by Angel Broking 4 on 24/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import UIKit


@objcMembers
class DBA_Ranking: NSObject {

    var Id:Int
    var ranking:String
    
    init(Id:Int,ranking:String)
    {
        self.Id = Id
        self.ranking = ranking
    }
    
}
