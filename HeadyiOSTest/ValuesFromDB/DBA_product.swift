//
//  DBA_product.swift
//  HeadyiOSTest
//
//  Created by Angel Broking 4 on 23/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import UIKit

@objcMembers
class DBA_product: NSObject {

    var Id:Int
    var name:String
    var date_added:String
    var categories_id : Int
    
    
    init(Id:Int,name:String,date_added:String,categories_id:Int )
    {
        self.Id = Id
        self.name = name
        self.date_added = date_added
        self.categories_id = categories_id
    }
}
