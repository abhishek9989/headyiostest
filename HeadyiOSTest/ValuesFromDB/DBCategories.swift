//
//  DBCategories.swift
//  HeadyiOSTest
//
//  Created by Angel Broking 4 on 23/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import UIKit


@objcMembers
class DBCategories:NSObject {
    
    var Id: Int
    var name:String
    
     init(Id:Int , name:String) {
        self.Id = Id
        self.name = name
        
    }
}
