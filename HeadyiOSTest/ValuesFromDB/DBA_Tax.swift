//
//  DBA_Tax.swift
//  HeadyiOSTest
//
//  Created by Angel Broking 4 on 24/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import UIKit
@objcMembers
class DBA_Tax: NSObject {

    var name:String
    var value:Float
    var a_Product_id : Int
    
    init(name:String,value:Float ,a_Product_id:Int )
    {
        self.name = name
        self.value = value
        self.a_Product_id = a_Product_id
    }
    
    
}
