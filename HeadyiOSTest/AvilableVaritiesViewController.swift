//
//  AvilableVaritiesViewController.swift
//  HeadyiOSTest
//
//  Created by Angel Broking 4 on 24/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import UIKit

class AvilableVaritiesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    
    

    @IBOutlet weak var detailTableView: UITableView!
    
    var a_Prodict_Id:Int32!
    var aVarient :[Any]?
    let cellReuseIdentifier = "AvilableVarietiesTableViewCell"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        aVarient = DBManagerOBJC.getSharedInstance().getVarientsDatabyA_Product_Id(a_Prodict_Id)as! [DBA_VarientsProduct]
        detailTableView.dataSource = self
        detailTableView.delegate = self
        detailTableView.tableFooterView = UIView()
        detailTableView.reloadData()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
       self.title = "Avilable Varity"
    }
    
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return aVarient?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 105.0 ;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:AvilableVarietiesTableViewCell  = self.detailTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! AvilableVarietiesTableViewCell!
        
        // set the text from the data model
        let obj  = aVarient![indexPath.row] as! DBA_VarientsProduct
       
        cell.lblColorName.text = obj.color
        cell.lblSizeValue.text =  String(format: "%d", obj.size)
        cell.lblPriceValue.text =  String(format: "%d", obj.price)
        
        
        return cell
        
    }
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
