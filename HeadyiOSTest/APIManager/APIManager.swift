//
//  APIServices.swift
//  MVVMUnderstanding
//
//  Created by abhishek on 16/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import Foundation

class APIServiceManager
{
    let stubDataURL = "https://stark-spire-93433.herokuapp.com/json"
    
    
    func getData(complition:@escaping (_ Weather:Json4Swift_Base?, _ error:Error?)->Void) -> Void
    {
        // Get Data
        
        getJsonFromURL(urlString: stubDataURL) { (data, error) in
            
            guard let data_ = data ,error == nil
                else
            {
                print("Failed to get data")
                return
            }
            
            self.createWeatherObjectWith(json: data_, completion: { (json4Swift_Base, error) in
                
                if let error = error {
                    print("Failed to convert data")
                    return complition(nil, error)
                }
                return complition(json4Swift_Base, error)
            })
            
            
        }
        
        
        
    }
    
}



extension APIServiceManager
{
    private func getJsonFromURL (urlString : String , completion: @escaping(_ Data :Data? , _ error:Error?)->Void)
    {
        guard let url = URL(string: urlString) else
        {
            print("Not able to convert to URL String..")
            return
        }
        
        let urlRequests = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: urlRequests){
            (Data , _ ,error) in
            guard error == nil else {
                print("Error calling api")
                return completion(nil, error)
            }
            guard let responseData = Data else {
                print("Data is nil")
                return completion(nil, error)
            }
            completion(responseData, nil)
        }
        task.resume()
        
        
    }
    
    
    private func createWeatherObjectWith(json: Data, completion: @escaping (_ data: Json4Swift_Base?, _ error: Error?) -> Void) {
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let weather = try decoder.decode(Json4Swift_Base.self, from: json)
            return completion(weather, nil)
        } catch let error {
            print("Error creating current weather from JSON because: \(error.localizedDescription)")
            return completion(nil, error)
        }
    }
    
    
    
}









