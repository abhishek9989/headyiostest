//
//  DetailedDataTableViewCell.swift
//  HeadyiOSTest
//
//  Created by Angel Broking 4 on 23/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import UIKit

class DetailedDataTableViewCell: UITableViewCell {


    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
