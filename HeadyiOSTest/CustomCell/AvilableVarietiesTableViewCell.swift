//
//  AvilableVarietiesTableViewCell.swift
//  HeadyiOSTest
//
//  Created by Angel Broking 4 on 24/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import UIKit

class AvilableVarietiesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblColorName: UILabel!
    @IBOutlet weak var lblSizeValue: UILabel!
    @IBOutlet weak var lblPriceValue: UILabel!
    
    
    @IBOutlet weak var color: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var size: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
