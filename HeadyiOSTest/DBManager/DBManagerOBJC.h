//
//  testObj.h
//  HeadyiOSTest
//
//  Created by Angel Broking 4 on 22/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class DBCategories,DBA_product;


@interface DBManagerOBJC : NSObject
{
    NSString *databasePath;
}

+(DBManagerOBJC*)getSharedInstance;
-(BOOL)createDB;


-(BOOL)saveDataForCategories:(int)Id name:(NSString*)name;
-(BOOL)saveDataForA_Product:(int)ID name:(NSString*)name date_added:(NSString*)date_added categories_Id:(int)categories_Id;
-(BOOL)saveDataForVariaents:(int)ID color:(NSString*)color size:(int)size price:(int)price a_ProductId:(int)a_ProductId;
-(NSArray*)getThedatabyCategories;
-(NSArray*)getThedatabyCategories_Id:(int)Id;
-(NSArray*)getVarientsDatabyA_Product_Id:(int)Id;

-(BOOL)saveDataForRanking:(int)ID ranking:(NSString*)ranking ;
-(NSArray*)retriveDataOfRanking;


@end

NS_ASSUME_NONNULL_END
