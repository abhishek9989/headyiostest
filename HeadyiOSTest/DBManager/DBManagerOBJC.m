//
//  testObj.m
//  HeadyiOSTest
//
//  Created by Angel Broking 4 on 22/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

#import "DBManagerOBJC.h"
#import <sqlite3.h>
#import "HeadyiOSTest-Swift.h"

@implementation DBManagerOBJC

static DBManagerOBJC *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

+(DBManagerOBJC*)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
        
    }
    return sharedInstance;
}


-(BOOL)createDB{
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
   
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent: @"HeadyiOS.db"]];
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath:databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            
            const char *sql_stmt = "create table if not exists Categories (id integer primary key, name text);"
            "create table if not exists A_Product (id integer primary key, name text, date_added text,categories_Id integer);"
            "create table if not exists Variants (id integer primary key, color text, size integer, price integer,a_ProductId integer);"
            "create table if not exists Tax (name text, value float,categories_Id integer);"
            "create table if not exists Ranking (id integer ,ranking text);"
            ;
            
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg)
                != SQLITE_OK)
            {
                isSuccess = NO;
            }
            sqlite3_close(database);
            
            database = nil;
            return  isSuccess;
            
        }
        else {
            isSuccess = NO;
        }
    }
    else
    {
 
    }
    return isSuccess;
}


-(int)openDB
{
    // BOOL isSuccess = false;
    
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent: @"HeadyiOS.db"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if (database)
    {
        return SQLITE_OK;
    }
    if ([filemgr fileExistsAtPath: databasePath ] == YES)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            return SQLITE_OK;
        }
    }
    else{
        return  SQLITE_CANTOPEN;
    }
    return SQLITE_CANTOPEN;
}



-(BOOL)saveDataForCategories:(int)Id name:(NSString*)name;
{
    const char *dbpath = [databasePath UTF8String];
    
    if ( SQLITE_OK == [self openDB])
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"insert into Categories (id,name) values(\"%d\",\"%@\")",
                               Id,
                               name];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_reset(statement);
            sqlite3_close(database);
            return YES;
        }
        else {
            sqlite3_reset(statement);
            sqlite3_close(database);
            return NO;
        }
    }
    return NO;
    
}


-(BOOL)saveDataForA_Product:(int)ID name:(NSString*)name date_added:(NSString*)date_added categories_Id:(int)categories_Id
{
    
    if ( SQLITE_OK == [self openDB])
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"insert into A_Product (id,name,date_added,categories_Id) values(\"%d\",\"%@\",\"%@\",\"%d\")",
                               ID,
                               name,
                               date_added,categories_Id];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_reset(statement);
            sqlite3_close(database);
            return YES;
        }
        else {
            sqlite3_reset(statement);
            sqlite3_close(database);
            return NO;
        }
    }
    return NO;
    
}


-(BOOL)saveDataForVariaents:(int)ID color:(NSString*)color size:(int)size price:(int)price a_ProductId:(int)a_ProductId
{
    if ( SQLITE_OK == [self openDB])
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"insert into Variants (id,color,size,price,a_ProductId) values(\"%d\",\"%@\",\"%d\",\"%d\",\"%d\")",
                               ID,
                               color,
                               size,price,a_ProductId];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_reset(statement);
            sqlite3_close(database);
            return YES;
        }
        else {
            sqlite3_reset(statement);
            sqlite3_close(database);
            return NO;
        }
    }
    return NO;
}


-(BOOL)saveDataForTax:(NSString*)name value:(float)value categories_Id:(int)categories_Id;
{
    const char *dbpath = [databasePath UTF8String];
    
    if ( SQLITE_OK == [self openDB])
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"insert into Tax (id,name) values(\"%@\",\"%f\",\"%d\")",
                               name,
                               value,categories_Id];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_reset(statement);
            sqlite3_close(database);
            return YES;
        }
        else {
            sqlite3_reset(statement);
            sqlite3_close(database);
            return NO;
        }
    }
    return NO;
    
}







-(NSArray*)getThedatabyCategories
{
    
    NSMutableArray *retval = [[NSMutableArray alloc] init] ;
    
    NSString *query = [NSString stringWithFormat:@"SELECT* FROM Categories ORDER BY id"];
    
    //sqlite3_stmt *statement;
    
    const char *dbpath = [databasePath UTF8String];
    
    if ( SQLITE_OK == [self openDB])
    {
        if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                
                int Id                       = sqlite3_column_int(statement, 0);
                char *name                   = (char *) sqlite3_column_text(statement, 1);
                
                DBCategories *categories = [[DBCategories alloc]initWithId:Id name:[[NSString alloc]initWithUTF8String:name]];
                [retval addObject:categories];
                categories = nil;
            }
            sqlite3_reset(statement);
            sqlite3_close(database);
        }
        else{
            
            NSLog(@"sqlite3_errmsg getTransportCrews Inside ....  %s",sqlite3_errmsg(database));
            sqlite3_reset(statement);
            sqlite3_close(database);
        }
        
    }
    else{
        
        NSLog(@"sqlite3_errmsg retiving getTransportCrews Data Outside ....  %s",sqlite3_errmsg(database));
        sqlite3_reset(statement);
        sqlite3_close(database);
    }
    
    return retval;
    
}



-(NSArray*)getThedatabyCategories_Id:(int)Id
{
    
    NSMutableArray *retval = [[NSMutableArray alloc] init] ;
    
    NSString *query = [NSString stringWithFormat:@"SELECT* FROM A_Product where categories_Id = \"%d\" ORDER BY id", Id];
    
    //sqlite3_stmt *statement;
    
    const char *dbpath = [databasePath UTF8String];
    
    if ( SQLITE_OK == [self openDB])
    {
        if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                
                int Id                       = sqlite3_column_int(statement, 0);
                char *name                   = (char *) sqlite3_column_text(statement, 1);
                char *date_added             = (char *) sqlite3_column_text(statement, 2);
                int categories_id            = sqlite3_column_int(statement, 3);
                
                DBA_product*dba_Product = [[DBA_product alloc]initWithId:Id name:[[NSString alloc]initWithUTF8String:name] date_added:[[NSString alloc]initWithUTF8String:date_added] categories_id:categories_id];
                [retval addObject:dba_Product];
                dba_Product = nil;
            }
            
            sqlite3_reset(statement);
            sqlite3_close(database);
        }
        else{
            
            NSLog(@"sqlite3_errmsg getTransportCrews Inside ....  %s",sqlite3_errmsg(database));
            sqlite3_reset(statement);
            sqlite3_close(database);
        }
        
    }
    else{
        
        NSLog(@"sqlite3_errmsg retiving getTransportCrews Data Outside ....  %s",sqlite3_errmsg(database));
        sqlite3_reset(statement);
        sqlite3_close(database);
    }
    
    return retval;
    
}


-(NSArray*)getVarientsDatabyA_Product_Id:(int)Id
{
    NSMutableArray *retval = [[NSMutableArray alloc] init] ;
    
    NSString *query = [NSString stringWithFormat:@"SELECT* FROM Variants where a_ProductId = \"%d\" ORDER BY id", Id];
    
    //sqlite3_stmt *statement;
    
    const char *dbpath = [databasePath UTF8String];
    
    if ( SQLITE_OK == [self openDB])
    {
        if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                
                int Id                       = sqlite3_column_int(statement, 0);
                char *color                   = (char *) sqlite3_column_text(statement, 1);
                int size             = sqlite3_column_int(statement, 2);
                int price            = sqlite3_column_int(statement, 3);
                int a_Product_id            = sqlite3_column_int(statement, 4);
                
                
                DBA_VarientsProduct*dba_Varients = [[DBA_VarientsProduct alloc]initWithId:Id color:[[NSString alloc]initWithUTF8String:color] size:size price:price a_Product_id:a_Product_id];
                
                [retval addObject:dba_Varients];
                dba_Varients = nil;
            }
            
            //sqlite3_finalize(statement);
            sqlite3_reset(statement);
            sqlite3_close(database);
        }
        else{
            
            NSLog(@"sqlite3_errmsg getTransportCrews Inside ....  %s",sqlite3_errmsg(database));
            sqlite3_reset(statement);
            sqlite3_close(database);
        }
        
    }
    else{
        
        NSLog(@"sqlite3_errmsg retiving getTransportCrews Data Outside ....  %s",sqlite3_errmsg(database));
        sqlite3_reset(statement);
        sqlite3_close(database);
    }
    
    return retval;
    
}



// Save Data forr rankings

-(BOOL)saveDataForRanking:(int)ID ranking:(NSString*)ranking
{
    if ( SQLITE_OK == [self openDB])
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"insert into ranking (id,ranking) values(\"%d\",\"%@\")",
                               ID,
                               ranking];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_reset(statement);
            sqlite3_close(database);
            return YES;
        }
        else {
            sqlite3_reset(statement);
            sqlite3_close(database);
            return NO;
        }
    }
    return NO;
}

-(NSArray*)retriveDataOfRanking
{
    const char *dbpath = [databasePath UTF8String];
    
    
    NSMutableArray *retval = [[NSMutableArray alloc] init] ;
    
    NSString *query = [NSString stringWithFormat:@"SELECT* FROM Ranking"];
    
    
    if ( SQLITE_OK == [self openDB])
    {
        if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                
                int Id                       = sqlite3_column_int(statement, 0);
                char *ranking                   = (char *) sqlite3_column_text(statement, 1);
                
                DBA_Ranking * dba_Ranking = [[DBA_Ranking alloc]initWithId:Id ranking:[[NSString alloc]initWithUTF8String:ranking]];
                
                [retval addObject:dba_Ranking];
                dba_Ranking = nil;
            }
            
            sqlite3_reset(statement);
            sqlite3_close(database);
        }
        else{
            
            NSLog(@"sqlite3_errmsg getTransportCrews Inside ....  %s",sqlite3_errmsg(database));
            sqlite3_reset(statement);
            sqlite3_close(database);
        }
    }
    
    return retval;
}





-(void)closedatabase
{
    if(database)
    {
        sqlite3_close(database);
    }
}








@end
