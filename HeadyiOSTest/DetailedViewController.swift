//
//  DetailedViewController.swift
//  HeadyiOSTest
//
//  Created by Angel Broking 4 on 23/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import UIKit

class DetailedViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate {
   
    

    var categories_Id:Int32!
    var aProductCount :[Any]?
    let cellReuseIdentifier = "DetailedDataTableViewCell"
    

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print(categories_Id)
        
        
        aProductCount = DBManagerOBJC.getSharedInstance().getThedatabyCategories_Id(categories_Id)as! [DBA_product]
        
        self.setupUI()
        
    }
    
    func setupUI() -> Void {
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
        tableView.tableFooterView = UIView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Details"
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return aProductCount?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // create a new cell if needed or reuse an old one
        let cell:DetailedDataTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! DetailedDataTableViewCell!
        
        // set the text from the data model
        let obj  = aProductCount![indexPath.row] as! DBA_product
        cell.lblName?.text = obj.name
        
        return cell
    }
    
    @objc func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        // set the text from the data model
        let obj  = aProductCount![indexPath.row] as! DBA_product
        self.goToNextPage(Id: Int32(obj.Id))
    }
    
    func goToNextPage(Id:Int32) -> Void {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AvilableVaritiesViewController") as? AvilableVaritiesViewController
        vc?.a_Prodict_Id =  Id
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
