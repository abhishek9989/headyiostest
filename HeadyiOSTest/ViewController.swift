//
//  ViewController.swift
//  HeadyiOSTest
//
//  Created by abhishek on 20/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITableViewDelegate,UITableViewDataSource {
  
    private let apiManager = APIServiceManager()
    
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnRatings: UIButton!
    let cellReuseIdentifier = "CustomTableViewCell"
    
    var categoriescCount: [Any]? = []
    
    var rankingCount:[Any]? = []
    var isCtegoriesClicked : Bool = false
    var isRankingClicked : Bool = false
    
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupUI()
        activityIndicator("Wait..")
        callAPiManager()
    }
    
    func setupUI() -> Void {
        // Setup UI
        btnCategory.layer.cornerRadius = 8
        btnRatings.layer.cornerRadius = 8
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
    }
    
    func activityIndicator(_ title: String) {
        print(#function)
        strLabel.removeFromSuperview()
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()
        
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 46))
        strLabel.text = title
        strLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        strLabel.textColor = UIColor(white: 0.9, alpha: 0.7)
        
        effectView.frame = CGRect(x: view.frame.midX - strLabel.frame.width/2, y: view.frame.midY - strLabel.frame.height/2 , width: 160, height: 46)
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true
        
        activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator.startAnimating()
        
        effectView.contentView.addSubview(activityIndicator)
        effectView.contentView.addSubview(strLabel)
        view.addSubview(effectView)
    }
    
    func removeEffectView() {
        print(#function)
        DispatchQueue.main.async {
            self.effectView.removeFromSuperview()
        }
    }
    
    
    
    func callAPiManager() -> Void {
        
        apiManager.getData { (json4Swift_Base, error) in
            
            if let error = error {
                print("Get weather error: \(error.localizedDescription)")
                return
            }
            guard let fashionData = json4Swift_Base  else
            {
                print("Something went wrong")
                return
            }
            print("Current Weather Object:")
            //print(fashionData)
            let categoreies:[Any] = fashionData.categories!
            let ranking :[Any] = fashionData.rankings!
            
            self.itreateCategoiesObject(categoreies as! [Categories])
            self.itreateRankingObject(ranking as![Rankings])
            self.removeEffectView()
            
            //print(ranking)
        }
    }
    
    
    func itreateRankingObject(_ dataArray:[Rankings]) -> Void
    {
        var Id : Int = 0
        dataArray.forEach { item in
            Id = Id+1
            self.saveDataForRanking(Id:Id, ranking: item.ranking!)
           
        }
    }
    
    
    func saveDataForRanking(Id:Int , ranking:String) -> Void
    {
        let isSuccess = DBManagerOBJC.getSharedInstance().saveData(forRanking:Int32(Id), ranking: ranking)
    }
    
    
    
    func itreateCategoiesObject(_ dataArray:[Categories]) -> Void
    {
        dataArray.forEach { item in
            self.saveMainCategories(name: item.name!, id: item.id!)
            self.setDataOfProduct(item.products!,Id: item.id!)
        }
    }
    
    func setDataOfProduct(_ dataArray:[A_products], Id:Int) -> Void
    {
        dataArray.forEach { item in
            
            saveDataForProducts(name: item.name!, id: item.id!, date_added: item.date_added ?? "No Date Added" , categories_Id:Id )
            self.setDataForVariaents(item.variants!,Id: item.id!)
        }
    }
    
    func setDataForVariaents(_ dataArray:[Variants],Id:Int) -> Void {
        
        dataArray.forEach { items in
            saveDataForVarients(color: items.color ?? "no Color", id: items.id ?? 0, size: items.size ?? 0, price: items.price ?? 0, a_ProductId: Id )
            
        }
        
    }
    
    
    func saveMainCategories(name: String, id:Int) {
        
        let isSuccess = DBManagerOBJC.getSharedInstance().saveData(forCategories: Int32(id), name: name)
    }
    func saveDataForProducts(name: String, id:Int, date_added:String ,categories_Id:Int) {
        
        let isA_ProductInserted = DBManagerOBJC.getSharedInstance().saveData(forA_Product: Int32(id), name: name, date_added: date_added ,categories_Id:Int32(categories_Id) )
        
    }
    
    
    
    func saveDataForVarients(color: String, id:Int, size:Int,price:Int,a_ProductId:Int ) {
        
        let insertVarients = DBManagerOBJC.getSharedInstance().saveData(forVariaents: Int32(id), color: color, size: Int32(size), price: Int32(price),a_ProductId:Int32(a_ProductId))
    }
    
    
    @IBAction func shopByCategoriesAction(_ sender: Any)
    {
        // Shop By categorries
        isCtegoriesClicked = true
        isRankingClicked = false
        categoriescCount = DBManagerOBJC.getSharedInstance().getThedatabyCategories() as! [DBCategories]
        print(categoriescCount!.count )
        self.tableView.reloadData()
        
    }
    
    
    @IBAction func shopByRatingsAction(_ sender: Any)
    {
        // Shop by ratings
        isCtegoriesClicked = false
        isRankingClicked = true
        rankingCount = DBManagerOBJC.getSharedInstance().retriveDataOfRanking() as! [DBA_Ranking]
        print(rankingCount!.count)
        self.tableView.reloadData()
    }
    
    
    @objc func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isRankingClicked == true {
            return rankingCount?.count ?? 0
        }
        else if isCtegoriesClicked == true
        {
            return categoriescCount?.count ?? 0
        }
        else
        {
            return 0
        }
    }
    
    @objc(tableView:cellForRowAtIndexPath:) func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell:CustomTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CustomTableViewCell!
        
        // set the text from the data model
        if isRankingClicked == true {
            
            let obj  = rankingCount![indexPath.row] as! DBA_Ranking
            cell.lblName?.text = obj.ranking
        }
        else if isCtegoriesClicked == true
        {
            let obj  = categoriescCount![indexPath.row] as! DBCategories
            cell.lblName?.text = obj.name
        }
        
        
        return cell
        
    }
    
    @objc func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("You tapped cell number \(indexPath.row).")
        // set the text from the data model
        
        if isRankingClicked == true
        {
            // Dont have details now
        }
        else if isCtegoriesClicked == true
        {
            let obj  = categoriescCount![indexPath.row] as! DBCategories
            self.goToNextPage(Id: Int32(obj.Id))
        }
    }

    
    func goToNextPage(Id:Int32) -> Void {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetailedViewController") as? DetailedViewController
        vc?.categories_Id =  Id
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
    
    
}












