
//
//  
//  HeadyiOSTest
//
//  Created by abhishek on 20/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//
import Foundation
struct Products : Codable {
	let id : Int?
	let view_count : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case view_count = "view_count"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		view_count = try values.decodeIfPresent(Int.self, forKey: .view_count)
	}

}
