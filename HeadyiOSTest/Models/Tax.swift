
//
// 
//  HeadyiOSTest
//
//  Created by abhishek on 20/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import Foundation
struct Tax : Codable {
	let name : String?
	let value : Double?

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case value = "value"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		value = try values.decodeIfPresent(Double.self, forKey: .value)
	}

}
