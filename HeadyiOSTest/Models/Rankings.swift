//
//  
//  HeadyiOSTest
//
//  Created by abhishek on 20/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import Foundation
struct Rankings : Codable {
	let ranking : String?
	let products : [Products]?

	enum CodingKeys: String, CodingKey {

		case ranking = "ranking"
		case products = "products"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		ranking = try values.decodeIfPresent(String.self, forKey: .ranking)
		products = try values.decodeIfPresent([Products].self, forKey: .products)
	}

}
