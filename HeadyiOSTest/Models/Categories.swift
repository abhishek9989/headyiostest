
//
//  
//  HeadyiOSTest
//
//  Created by abhishek on 20/05/19.
//  Copyright © 2019 abhishek. All rights reserved.
//

import Foundation

struct Categories : Codable {
    
	let id : Int?
	let name : String?
	let products : [A_products]?
	let child_categories : [String]?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case products = "products"
		case child_categories = "child_categories"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		products = try values.decodeIfPresent([A_products].self, forKey: .products)
		child_categories = try values.decodeIfPresent([String].self, forKey: .child_categories)
	}

}
